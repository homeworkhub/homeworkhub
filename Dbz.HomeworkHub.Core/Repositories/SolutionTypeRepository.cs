﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace Dbz.HomeworkHub.Core.Repositories
{
    /// <summary>
    /// Repository Klasse für die Verwaltung von Lösungstypen
    /// </summary>
    public class SolutionTypeRepository : BaseRepository<Entities.SolutionType>
    {
        #region Methoden

        /// <summary>
        /// Standardkonstruktor
        /// </summary>
        /// <param name="businessObjects">Die Business-Objekts welchem dieses Repository zugeordnet wurde</param>
        internal SolutionTypeRepository(BusinessObjects businessObjects)
            : base(businessObjects)
        {
        }

        /// <summary>
        /// Legt einen neuen Lösungstypen an
        /// </summary>
        /// <param name="name">Name des neuen Lösungstypen</param>
        /// <returns>Instanz des neuen Lösungstyps</returns>
        public Entities.SolutionType New(string name)
        {
            Entities.SolutionType instance = new Entities.SolutionType()
            {
                Name = name
            };

            // Instanz in der Datenbank speichern
            BusinessObjects.Session.SaveOrUpdate(instance);

            return instance;
        }

        #endregion
    }
}
