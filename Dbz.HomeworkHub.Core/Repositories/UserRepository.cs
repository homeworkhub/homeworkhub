﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Linq;

namespace Dbz.HomeworkHub.Core.Repositories
{
    /// <summary>
    /// Repository Klasse für die Verwaltung von Benutezrs
    /// </summary>
    public class UserRepository : BaseRepository<Entities.User>
    {
        #region Methoden

        /// <summary>
        /// Standardkonstruktor
        /// </summary>
        /// <param name="businessObjects">Die Business-Objekts welchem dieses Repository zugeordnet wurde</param>
        internal UserRepository(BusinessObjects businessObjects)
            : base(businessObjects)
        {
        }

        /// <summary>
        /// Legt einen neuen Benutzer an
        /// </summary>
        /// <param name="email">E-Mailaddresse des neuen Benutzers</param>
        /// <param name="firstname">Vorname des neuen Benutzers</param>
        /// <param name="lastname">Nachname des neuen Benutzers</param>
        /// <param name="password">Passwort des neuen Benutzers</param>
        /// <returns>Instanz des neuen Benutzers</returns>
        public Entities.User New(string email, string firstname, string lastname, string password)
        {
            // Neue Instanz eines Benutzerobjektes anlegen
            Entities.User user = new Entities.User()
            {
                Firstname = firstname,
                Lastname = lastname,
                EMail = email,
                Password = password,
                CreditPoints = 0,
                RegistrationDate = DateTime.Now,
                DateOfBirth = null
            };

            // Instanz in der Datenbank speichern
            BusinessObjects.Session.SaveOrUpdate(user);

            return user;
        }

        /// <summary>
        /// Fügt dem Benutzer einen neuen Benutzerrang für ein Thema mit einem Initalwert für 0
        /// </summary>
        /// <param name="user">User für welchend er Rang erstellt wird</param>
        /// <param name="topic">Thema welches für den Rang verwendet iwrd</param>
        /// <returns>Instanz des neuen Benutzerrangs</returns>
        public Entities.UserRank AddUserRank(Entities.User user, Entities.Topic topic)
        {
            Entities.UserRank instance = BusinessObjects.UserRanks.New(user, topic, 0.0);
            user.Ranks.Add(instance);
            return instance;
        }

        /// <summary>
        /// Entfernt die angebebene Instanz eines Benutzerranges
        /// </summary>
        /// <param name="userRank">Instanz des zu löschenden Benutzerranges</param>
        public void RemoveUserRank(Entities.UserRank userRank)
        {
            BusinessObjects.UserRanks.Delete(userRank);
        }

        /// <summary>
        /// Liefert einen Benutzer anhand seiner E-Mailaddrese
        /// </summary>
        /// <param name="email">Die E-Mailaddresse welche für die Abfrage verwendet wird</param>
        /// <returns>Instanz eines Benutzers oder <c>null</c> wenn kein Benutzer gefunden wurde.</returns>
        public Entities.User GetUserByEMail(string email)
        {
            return (from u in BusinessObjects.Session.Query<Entities.User>() where u.EMail.StartsWith(email) select u).FirstOrDefault();
        }

        /// <summary>
        /// Liefert einen Benutzer anhand seiner E-Mailaddrese und seinem Passwort
        /// </summary>
        /// <param name="email">Die E-Mailaddresse welche für die Abfrage verwendet wird</param>
        /// <param name="password">Das Passwort welche für die Abfrage verwendet wird</param>
        /// <returns>Instanz eines Benutzers oder <c>null</c> wenn kein Benutzer gefunden wurde.</returns>
        public Entities.User GetUserByEMailAndPassword(string email, string password)
        {
            return (from u in BusinessObjects.Session.Query<Entities.User>() where u.EMail.StartsWith(email) && u.Password == password select u).FirstOrDefault();
        }

        #endregion
    }
}
