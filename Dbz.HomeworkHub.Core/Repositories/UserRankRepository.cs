﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace Dbz.HomeworkHub.Core.Repositories
{
    /// <summary>
    /// Repository Klasse für die Verwaltung von Benutzerrängen
    /// </summary>
    public class UserRankRepository : BaseRepository<Entities.UserRank>
    {
        #region Methoden

        /// <summary>
        /// Standardkonstruktor
        /// </summary>
        /// <param name="businessObjects">Die Business-Objekts welchem dieses Repository zugeordnet wurde</param>
        internal UserRankRepository(BusinessObjects businessObjects)
            : base(businessObjects)
        {
        }

        /// <summary>
        /// Erstellt einen neuen Benutzerrang zu einer Benutzer-Themen Relation
        /// </summary>
        /// <param name="user">Der Benutzer welches für den Rang verwendet wird</param>
        /// <param name="topic">Das Thema welches für den Rang verwendet wird</param>
        /// <param name="initialRank">Der Initialrang welcher festgelegt wurde</param>
        /// <returns>Instanz des neuen Benutzerranges</returns>
        internal Entities.UserRank New(Entities.User user, Entities.Topic topic, double initialRank)
        {
            Entities.UserRank instance = new Entities.UserRank()
            {
                Topic = topic,
                User = user,
                Rank = initialRank
            };

            // Instanz in der Datenbank speichern
            BusinessObjects.Session.SaveOrUpdate(instance);

            return instance;
        }

        #endregion
    }
}
