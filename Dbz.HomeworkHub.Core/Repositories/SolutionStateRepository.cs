﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace Dbz.HomeworkHub.Core.Repositories
{
    /// <summary>
    /// Repository Klasse für die Verwaltung von Lösungsstatus
    /// </summary>
    public class SolutionStateRepository : BaseRepository<Entities.SolutionState>
    {
        #region Methoden

        /// <summary>
        /// Standardkonstruktor
        /// </summary>
        /// <param name="businessObjects">Die Business-Objekts welchem dieses Repository zugeordnet wurde</param>
        internal SolutionStateRepository(BusinessObjects businessObjects)
            : base(businessObjects)
        {
        }

        /// <summary>
        /// Legt einen neuen Lösungstatus an
        /// </summary>
        /// <param name="name">Name des neuen Lösungstatus</param>
        /// <returns>Instanz des neuen Lösungstatus</returns>
        public Entities.SolutionState New(string name)
        {
            Entities.SolutionState instance = new Entities.SolutionState()
            {
                Name = name
            };

            // Instanz in der Datenbank speichern
            BusinessObjects.Session.SaveOrUpdate(instance);

            return instance;
        }

        #endregion
    }
}
