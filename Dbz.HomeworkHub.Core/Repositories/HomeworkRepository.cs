﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Linq;

namespace Dbz.HomeworkHub.Core.Repositories
{
    /// <summary>
    /// Repository Klasse für die Verwaltung von Hausaufgaben
    /// </summary>
    public class HomeworkRepository : BaseRepository<Entities.Homework>
    {
        #region Methoden

        /// <summary>
        /// Standardkonstruktor
        /// </summary>
        /// <param name="businessObjects">Die Business-Objekts welchem dieses Repository zugeordnet wurde</param>
        internal HomeworkRepository(BusinessObjects businessObjects)
            : base(businessObjects)
        {
        }

        /// <summary>
        /// Legt eine neue Hausaufgabe an
        /// </summary>
        /// <remarks>
        /// Nicht für den Produktiven Einsatz
        /// </remarks>
        public Entities.Homework New(string name, DateTime lastSubmitDate, int neededTopicRank, string beschreibung, int userId, int topicId)
        {
            Entities.Homework instance = new Entities.Homework()
            {
                Name = name,
                LastSubmitDate = lastSubmitDate,
                Description = beschreibung,
                RequiredTopicRank = neededTopicRank,
                Creator = BusinessObjects.Users.GetOne(userId),
                Topic = BusinessObjects.Topics.GetOne(topicId),
                CreationDate = DateTime.Now,
                CreditPoints = 10,
            };

            // Instanz in der Datenbank speichern
            Update(instance);

            return instance;
        }

        public List<Entities.Homework> GetOpen()
        {
            return (from u in BusinessObjects.Session.Query<Entities.Homework>() where u.LastSubmitDate >= DateTime.Now && u.Subscriber == null select u).ToList();
        }

        public List<Entities.Homework> GetForSubscriber(int userId)
        {
            return (from u in BusinessObjects.Session.Query<Entities.Homework>() where u.Subscriber.UserId == userId select u).ToList();
        }
        public List<Entities.Homework> GetForCreator(int userId)
        {
            return (from u in BusinessObjects.Session.Query<Entities.Homework>() where u.Creator.UserId == userId select u).ToList();
        }

        public void Update(Entities.Homework topic)
        {
            BusinessObjects.Session.SaveOrUpdate(topic);
        }

        #endregion
    }
}
