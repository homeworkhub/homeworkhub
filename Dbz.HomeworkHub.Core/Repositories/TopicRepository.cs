﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace Dbz.HomeworkHub.Core.Repositories
{
    /// <summary>
    /// Repository Klasse für die Verwaltung von Themengebiete
    /// </summary>
    public class TopicRepository : BaseRepository<Entities.Topic>
    {
        #region Methoden

        /// <summary>
        /// Standardkonstruktor
        /// </summary>
        /// <param name="businessObjects">Die Business-Objekts welchem dieses Repository zugeordnet wurde</param>
        internal TopicRepository(BusinessObjects businessObjects)
            : base(businessObjects)
        {
        }

        /// <summary>
        /// Legt ein neues Themengebiet mit dem angegebenen Namen an
        /// </summary>
        /// <param name="name">Name des Themengebietes</param>
        /// <returns>Instanz des neuen Themengebietes</returns>
        public Entities.Topic New(string name)
        {
            Entities.Topic instance = new Entities.Topic()
            {
                Name = name
            };

            // Instanz in der Datenbank speichern
            Update(instance);

            return instance;
        }

        /// <summary>
        /// Speichert eine Instanz eines Themengebietes
        /// </summary>
        /// <param name="topic">Die Instanz eines Themengebietes welches gespeichert werden soll</param>
        public void Update(Entities.Topic topic)
        {
            BusinessObjects.Session.SaveOrUpdate(topic);
        }

        #endregion
    }
}
