﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace Dbz.HomeworkHub.Core.Repositories
{
    /// <summary>
    /// Repository Klasse für die Verwaltung von Lösungen
    /// </summary>
    public class SolutionRepository : BaseRepository<Entities.Solution>
    {
        #region Methoden

        /// <summary>
        /// Standardkonstruktor
        /// </summary>
        /// <param name="businessObjects">Die Business-Objekts welchem dieses Repository zugeordnet wurde</param>
        internal SolutionRepository(BusinessObjects businessObjects)
            : base(businessObjects)
        {
        }

        /// <summary>
        /// Legt eine neue Lösung an
        /// </summary>
        /// <remarks>
        /// Nicht für den Produktiven Einsatz
        /// </remarks>
        public Entities.Solution New()
        {
            return null;
        }

        #endregion
    }
}
