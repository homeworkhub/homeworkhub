﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Linq;

namespace Dbz.HomeworkHub.Core.Repositories
{
    /// <summary>
    /// Basisrepository welches Grundlegene Operationen auf der Datenbank für CRUD bereitstellt.
    /// </summary>
    /// <typeparam name="EntityType">Der Typ der Entitiät die durch ein spezifisches Repository verwaltet wird</typeparam>
    public abstract class BaseRepository<EntityType> where EntityType : new()
    {
        #region Properties

        /// <summary>
        /// Das Business-Object welches dem Repository zugeordnet ist
        /// </summary>
        protected BusinessObjects BusinessObjects { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="businessObjects">Businessobject für die Zuordnung</param>
        public BaseRepository(BusinessObjects businessObjects)
        {
            BusinessObjects = businessObjects;
        }

        /// <summary>
        /// Liefert alle Objekte des angebenenen Typs
        /// </summary>
        /// <returns>Liste der abgefragten Datenobjekte</returns>
        public virtual List<EntityType> GetAll()
        {
            return (from u in BusinessObjects.Session.Query<EntityType>() select u).ToList();
        }

        /// <summary>
        /// Liefert ein Objekte des angebenenen Typs
        /// </summary>
        /// <returns><c>null</c>oder das Objekt des Typs mit der angegebenen Id</returns>
        public virtual EntityType GetOne(object id)
        {
            return BusinessObjects.Session.Get<EntityType>(id);
        }

        /// <summary>
        /// Löscht ein spezifisches Objekt
        /// </summary>
        /// <returns>Das Objekt welches gelöscht werden soll</returns>
        public virtual void Delete(EntityType entity)
        {
            BusinessObjects.Session.Delete(entity);
        }

        #endregion
    }
}
