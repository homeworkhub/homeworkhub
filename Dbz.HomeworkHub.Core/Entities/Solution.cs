﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dbz.HomeworkHub.Core.Entities
{
    /// <summary>
    /// DAO-Klasse für Lösungen
    /// </summary>
    public class Solution
    {
        #region Properties

        /// <summary>
        /// Liefert die ID einer Lösung oder legt diese fest
        /// </summary>
        public virtual int SolutionId { get; set; }

        /// <summary>
        /// Liefert den Namen einer Lösung oder legt diesen fest
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Liefert den Inhalt einer Lösung oder legt diesen fest
        /// </summary>
        public virtual string SolutionText { get; set; }

        /// <summary>
        /// Liefert das Erstelldatum einer Lösung oder legt dieses fest
        /// </summary>
        public virtual DateTime CreationDate { get; set; }

        /// <summary>
        /// Liefert den Lösungstyp der Lösung oder legt diesen fest
        /// </summary>
        public virtual SolutionType SolutionType { get; set; }

        /// <summary>
        /// Liefert den Lösungsstatus der Lösung oder legt diesen fest
        /// </summary>
        public virtual SolutionState SolutionState { get; set; }

        /// <summary>
        /// Liefert die Hausaufgabe welcher die Lösung zugeordnet ist  oder legt diese fest
        /// </summary>
        public virtual Homework Homework { get; set; }

        /// <summary>
        /// Liefert den Ersteller der Lösung oder legt diese fest
        /// </summary>
        public virtual User Creator { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Standardkonstruktor
        /// </summary>
        public Solution()
        {
        }

        #endregion
    }
}
