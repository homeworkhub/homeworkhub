﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dbz.HomeworkHub.Core.Entities
{
    /// <summary>
    /// DAO-Klasse für Lösungsstatus
    /// </summary>
    public class SolutionState
    {
        #region Properties

        /// <summary>
        /// Liefert die ID des Lösungsstatus oder legt diese fest
        /// </summary>
        public virtual int SolutionStateId { get; set; }

        /// <summary>
        /// Liefert den Namen des Lösungsstatus oder legt diese fest
        /// </summary>
        public virtual string Name { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Standardkonstruktor
        /// </summary>
        public SolutionState()
        {
        }

        #endregion
    }
}
