﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dbz.HomeworkHub.Core.Entities
{
    /// <summary>
    /// DAO-Klasse für Themengebiete
    /// </summary>
    public class Topic
    {
        #region Properties

        /// <summary>
        /// Liefert die ID des Themengebietes oder legt diese fest
        /// </summary>
        public virtual int TopicId { get; set; }

        /// <summary>
        /// Liefert den Namen des Themengebietes oder legt diesen fest
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Liefert die Beschreibung des Themengebietes oder legt diese fest
        /// </summary>
        public virtual string Description { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Standardkonstruktor
        /// </summary>
        public Topic()
        {
        }

        #endregion
    }
}
