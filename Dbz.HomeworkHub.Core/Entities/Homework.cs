﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dbz.HomeworkHub.Core.Entities
{
    /// <summary>
    /// DAO-Klasse für Hausaufgaben
    /// </summary>
    public class Homework
    {
        #region Properties

        /// <summary>
        /// Liefert die ID der Hausaufgabe oder legt diese fest
        /// </summary>
        public virtual int HomeworkId { get; set; }

        /// <summary>
        /// Liefert den Namen der Hausaufgabe oder legt diese fest
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Liefert die Beschreibung der Hausaufgabe oder legt diese fest
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Liefert die CreditPoints der Hausaufgabe oder legt diese fest
        /// </summary>
        public virtual int CreditPoints { get; set; }

        /// <summary>
        /// Liefert den RequiredTopicRank der Hausaufgabe oder legt diese fest
        /// </summary>
        public virtual double RequiredTopicRank { get; set; }

        /// <summary>
        /// Liefert das CreationDate der Hausaufgabe oder legt diese fest
        /// </summary>
        public virtual DateTime CreationDate { get; set; }

        /// <summary>
        /// Liefert das LastSubmitDate der Hausaufgabe oder legt diese fest
        /// </summary>
        public virtual DateTime LastSubmitDate { get; set; }

        /// <summary>
        /// Liefert das SubscriptionDate der Hausaufgabe oder legt diese fest
        /// </summary>
        public virtual DateTime? SubscriptionDate { get; set; }

        /// <summary>
        /// Liefert das Thema, welchem die Hausaufgabe zugeordnet ist, oder legt dieses fest.
        /// </summary>
        public virtual Topic Topic { get; set; }

        /// <summary>
        /// Liefert den Benutzer der die Hausaufgabe erstellt hat, oder legt dieses fest
        /// </summary>
        public virtual User Creator { get; set; }

        /// <summary>
        /// Liefert den Benutzer der die Hausaufgabe abarbeitet, oder legt dieses fest
        /// </summary>
        public virtual User Subscriber { get; set; }

        /// <summary>
        /// Liefert die Liste der Lösungen der Hausaufgabe oder legt diese fest
        /// </summary>
        public virtual IList<Solution> Solutions { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Standardkonstruktor
        /// </summary>
        public Homework()
        {
        }

        #endregion
    }
}
