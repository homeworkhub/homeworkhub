﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dbz.HomeworkHub.Core.Entities
{
    /// <summary>
    /// DAO-Klasse für Benutzerrangs
    /// </summary>
    public class UserRank
    {
        #region Properties

        /// <summary>
        /// Liefert die ID des Benutzerrangs oder legt diese fest
        /// </summary>
        public virtual int UserRankId { get; set; }

        /// <summary>
        /// Liefert das Themengebiet auf welches sich der Benutzerrang bezieht
        /// </summary>
        public virtual Topic Topic { get; set; }

        /// <summary>
        /// Liefert den User auf welches sich der Benutzerrang bezieht
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Liefert den Rangwert des Benutzerrangs
        /// </summary>
        public virtual double Rank { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Standardkonstruktor
        /// </summary>
        public UserRank()
        {
        }

        #endregion
    }
}
