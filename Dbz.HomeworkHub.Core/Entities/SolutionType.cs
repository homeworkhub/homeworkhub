﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dbz.HomeworkHub.Core.Entities
{
    /// <summary>
    /// DAO-Klasse für Lösungstyp
    /// </summary>
    public class SolutionType
    {
        #region Properties

        /// <summary>
        /// Liefert die ID des Lösungstyp oder legt diese fest
        /// </summary>
        public virtual int SolutionTypeId { get; set; }

        /// <summary>
        /// Liefert den Namen des Lösungsstatus oder legt diesen fest
        /// </summary>
        public virtual string Name { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Standardkonstruktor
        /// </summary>
        public SolutionType()
        {
        }

        #endregion
    }
}
