﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dbz.HomeworkHub.Core.Entities
{
    /// <summary>
    /// DAO-Klasse für Benutzer
    /// </summary>
    public class User
    {
        #region Properties

        /// <summary>
        /// Liefert die ID eines Benutzers oder legt diese fest
        /// </summary>
        public virtual int UserId { get; set; }

        /// <summary>
        /// Liefert die E-Mailadresse eines Benutzers oder legt diese fest
        /// </summary>
        public virtual string EMail { get; set; }

        /// <summary>
        /// Liefert das Registrierungsdatum eines Benutzers oder legt dieses fest
        /// </summary>
        public virtual DateTime RegistrationDate { get; set; }

        /// <summary>
        /// Liefert das Geburtsdatum eines Benutzers oder legt dieses fest
        /// </summary>
        public virtual DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Liefert den Nachnamen eines Benutzers oder legt diesen fest
        /// </summary>
        public virtual string Lastname { get; set; }

        /// <summary>
        /// Liefert den Vornamen eines Benutzers oder legt diesen fest
        /// </summary>
        public virtual string Firstname { get; set; }

        /// <summary>
        /// Liefert das Passwort eines Benutzers oder legt dieses fest
        /// </summary>
        public virtual string Password { get; set; }

        /// <summary>
        /// Liefert die aktuellen Creditpoints Benutzers oder legt diese fest
        /// </summary>
        public virtual int CreditPoints { get; set; }

        /// <summary>
        /// Liefert die Liste aller Ränge zu Themen die dieser Benutzer besitzt, oder legt diese fest
        /// </summary>
        public virtual IList<UserRank> Ranks { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Standardkonstruktor
        /// </summary>
        public User()
        {
        }

        #endregion
    }
}
