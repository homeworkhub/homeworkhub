﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Linq;

namespace Dbz.HomeworkHub.Core
{
    /// <summary>
    /// Klasse stellt den zugriff auf die Datenbank bereit
    /// </summary>
    public class BusinessObjects : IDisposable
    {
        #region Properties

        /// <summary>
        /// Die Session die für alle Operationen auf die Datenbank verwendet wird
        /// </summary>
        internal ISession Session { get; set; }

        /// <summary>
        /// Repository stellt Methoden zur Verwaltung von Benutzerobjekten bereit
        /// </summary>
        public Repositories.UserRepository Users { get; set; }

        /// <summary>
        /// Repository stellt Methoden zur Verwaltung von Lösungenstatus bereit
        /// </summary>
        public Repositories.SolutionStateRepository SolutionStates { get; set; }

        /// <summary>
        /// Repository stellt Methoden zur Verwaltung Lösungstypen bereit
        /// </summary>
        public Repositories.SolutionTypeRepository SolutionTypes { get; set; }

        /// <summary>
        /// Repository stellt Methoden zur Verwaltung Themengebieten bereit
        /// </summary>
        public Repositories.TopicRepository Topics { get; set; }

        /// <summary>
        /// Repository stellt Methoden zur Hausaufgaben bereit
        /// </summary>
        public Repositories.HomeworkRepository Homeworks { get; set; }

        /// <summary>
        /// Repository stellt Methoden zur Verwaltung von Benutzerrängen
        /// </summary>
        internal Repositories.UserRankRepository UserRanks { get; set; }

        /// <summary>
        /// Repository stellt Methoden zur Verwaltung von Lösungen bereit
        /// </summary>
        internal Repositories.SolutionRepository Solutions { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Standardkonstruktor der Business-Objects
        /// </summary>
        public BusinessObjects()
        {
            // Session erhalten
            Session = Common.DataAccessProvider.NewSession();

            // Transaktion starten
            Session.BeginTransaction();

            // Repository-Objekte mit neuer Session erstellen
            Users = new Repositories.UserRepository(this);
            SolutionStates = new Repositories.SolutionStateRepository(this);
            SolutionTypes = new Repositories.SolutionTypeRepository(this);
            Topics = new Repositories.TopicRepository(this);
            UserRanks = new Repositories.UserRankRepository(this);
            Solutions = new Repositories.SolutionRepository(this);
            Homeworks = new Repositories.HomeworkRepository(this);
        }

        /// <summary>
        /// Schließt automatisch die Session und führt einen Commit durch
        /// </summary>
        public void Dispose()
        {
            try
            {
                Session.Transaction.Commit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Session.Transaction.Rollback();
            }
            finally
            {
                Session.Close();
                Session.Dispose();
            }
        }

        #endregion


    }
}
