﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Cfg;

namespace Dbz.HomeworkHub.Core.Common
{
    public static class DataAccessProvider
    {
        #region Properties

        private static NHibernate.ISessionFactory SessionFactory { get; set; }

        #endregion

        #region Methods

        static DataAccessProvider()
        {
            Initialize();
        }

        private static void Initialize()
        {
            Configuration config = new Configuration();
            config.AddAssembly(System.Reflection.Assembly.GetExecutingAssembly());
            SessionFactory = config.BuildSessionFactory();
        }

        public static ISession NewSession()
        {
            ISession session = SessionFactory.OpenSession();
            session.FlushMode = FlushMode.Commit;
            return session;
        }

        #endregion
    }
}
