﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dbz.HomeworkHub.Core.Test
{
    [TestClass]
    public class UserTests
    {
        [TestMethod]
        public void RequestSpecificUser()
        {
            using (Dbz.HomeworkHub.Core.BusinessObjects bo = new BusinessObjects())
            {
                var user = bo.Users.GetOne(-1);
                Assert.IsNull(user);
            }
        }


        [TestMethod]
        public void RequestAllUser()
        {
            using (Dbz.HomeworkHub.Core.BusinessObjects bo = new BusinessObjects())
            {
                var user = bo.Users.GetAll();
                Assert.IsTrue(user.Count > 0);
            }
        }

        [TestMethod]
        public void CreateInvalidUser()
        {
            Dbz.HomeworkHub.Core.Entities.User user = null;

            using (Dbz.HomeworkHub.Core.BusinessObjects bo = new BusinessObjects())
                bo.Users.New("UnittestUser@test.de", "", "", "");
           
            using (Dbz.HomeworkHub.Core.BusinessObjects bo = new BusinessObjects())
                user = bo.Users.GetUserByEMail("UnittestUser@test.de");
            
            Assert.IsNotNull(user);
        }
    }
}
