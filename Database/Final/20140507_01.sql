
CREATE TABLE [dbo].[Homework](
	[HomeworkId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[CreditPoints] [int] NOT NULL,
	[RequiredTopicRank] [float] NOT NULL,
	[TopicId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastSubmitDate] [datetime] NOT NULL,
	[CreatorId] [int] NOT NULL,
	[SubscriberId] [int] NULL,
	[SubscriptionDate] [datetime] NULL,
 CONSTRAINT [PK_Homework] PRIMARY KEY CLUSTERED 
(
	[HomeworkId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Solution]    Script Date: 07.05.2014 11:09:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Solution](
	[SolutionId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[SolutionText] [nvarchar](max) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[SolutionTypeId] [int] NOT NULL,
	[SolutionStateId] [int] NULL,
	[HomeworkdId] [int] NOT NULL,
	[CreatorId] [int] NOT NULL,
 CONSTRAINT [PK_Solution] PRIMARY KEY CLUSTERED 
(
	[SolutionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SolutionState]    Script Date: 07.05.2014 11:09:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SolutionState](
	[SolutionStateId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
 CONSTRAINT [PK_SolutionState] PRIMARY KEY CLUSTERED 
(
	[SolutionStateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SolutionType]    Script Date: 07.05.2014 11:09:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SolutionType](
	[SolutionTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_SolutionType] PRIMARY KEY CLUSTERED 
(
	[SolutionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Topic]    Script Date: 07.05.2014 11:09:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Topic](
	[TopicId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Topic] PRIMARY KEY CLUSTERED 
(
	[TopicId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 07.05.2014 11:09:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[RegistrationDate] [datetime] NOT NULL,
	[EMail] [nvarchar](255) NOT NULL,
	[DateOfBirth] [datetime] NULL,
	[Firstname] [nvarchar](255) NULL,
	[Lastname] [nvarchar](255) NULL,
	[CreditPoints] [int] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRank]    Script Date: 07.05.2014 11:09:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRank](
	[UserRankId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TopicId] [int] NOT NULL,
	[Rank] [float] NOT NULL,
 CONSTRAINT [PK_UserRank] PRIMARY KEY CLUSTERED 
(
	[UserRankId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Homework]  WITH CHECK ADD  CONSTRAINT [FK_Homework_Creator] FOREIGN KEY([CreatorId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Homework] CHECK CONSTRAINT [FK_Homework_Creator]
GO
ALTER TABLE [dbo].[Homework]  WITH CHECK ADD  CONSTRAINT [FK_Homework_Subscriber] FOREIGN KEY([SubscriberId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Homework] CHECK CONSTRAINT [FK_Homework_Subscriber]
GO
ALTER TABLE [dbo].[Homework]  WITH CHECK ADD  CONSTRAINT [FK_Homework_Topic] FOREIGN KEY([TopicId])
REFERENCES [dbo].[Topic] ([TopicId])
GO
ALTER TABLE [dbo].[Homework] CHECK CONSTRAINT [FK_Homework_Topic]
GO
ALTER TABLE [dbo].[Solution]  WITH CHECK ADD  CONSTRAINT [FK_Solution_Homework] FOREIGN KEY([HomeworkdId])
REFERENCES [dbo].[Homework] ([HomeworkId])
GO
ALTER TABLE [dbo].[Solution] CHECK CONSTRAINT [FK_Solution_Homework]
GO
ALTER TABLE [dbo].[Solution]  WITH CHECK ADD  CONSTRAINT [FK_Solution_SolutionState] FOREIGN KEY([SolutionStateId])
REFERENCES [dbo].[SolutionState] ([SolutionStateId])
GO
ALTER TABLE [dbo].[Solution] CHECK CONSTRAINT [FK_Solution_SolutionState]
GO
ALTER TABLE [dbo].[Solution]  WITH CHECK ADD  CONSTRAINT [FK_Solution_SolutionType] FOREIGN KEY([SolutionTypeId])
REFERENCES [dbo].[SolutionType] ([SolutionTypeId])
GO
ALTER TABLE [dbo].[Solution] CHECK CONSTRAINT [FK_Solution_SolutionType]
GO
ALTER TABLE [dbo].[Solution]  WITH CHECK ADD  CONSTRAINT [FK_Solution_User] FOREIGN KEY([CreatorId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Solution] CHECK CONSTRAINT [FK_Solution_User]
GO
ALTER TABLE [dbo].[UserRank]  WITH CHECK ADD  CONSTRAINT [FK_UserRank_Topic] FOREIGN KEY([TopicId])
REFERENCES [dbo].[Topic] ([TopicId])
GO
ALTER TABLE [dbo].[UserRank] CHECK CONSTRAINT [FK_UserRank_Topic]
GO
ALTER TABLE [dbo].[UserRank]  WITH CHECK ADD  CONSTRAINT [FK_UserRank_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserRank] CHECK CONSTRAINT [FK_UserRank_User]
GO
USE [master]
GO
ALTER DATABASE [DBZ_DB] SET  READ_WRITE 
GO
