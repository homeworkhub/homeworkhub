﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dbz.HomeworkHub.Frontend.Models
{
    public class CurrentUserModel : BaseModel
    {
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public string EMail { get; set; }
        public bool IsAdmin { get; set; }

        public int UserId { get; set; }
    }
}