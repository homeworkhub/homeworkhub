﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dbz.HomeworkHub.Frontend.Models
{
    public class BaseModel
    {
        public List<string> Errors { get; set; }

        public bool ErrorOccured
        {
            get
            {
                return Errors.Count > 0;
            }
        }

        public BaseModel()
        {
            Errors = new List<string>();
        }
    }
}