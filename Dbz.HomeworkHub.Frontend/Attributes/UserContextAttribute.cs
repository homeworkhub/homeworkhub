﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Dbz.HomeworkHub.Frontend.Models;

namespace Dbz.HomeworkHub.Frontend.Attributes
{
    /// <summary>
    /// Wird für jede ControllerMethode aufgerufen und ermittelt den aktuell angemeldeten Benutzer eines Request, welcher dann automatisch
    /// der View-Bag zugeordnet wird.
    /// </summary>
    public class UserContextAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Wird aufgeurfen nachdem der Controller ausgeführt wurde und fügt dem Ergebnis den aktuell angemeldeten Benutzer hinzu
        /// </summary>
        /// <param name="filterContext">Context in welchem der Filter ausgeführt wird</param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                var bo = Controllers.BaseController.GetBusinessObject(System.Web.HttpContext.Current.Request);
                if (bo != null)
                {
                    string username = FormsAuthentication.Decrypt(filterContext.HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                    var user = bo.Users.GetUserByEMail(username);
                    if(user != null)
                    {
                        CurrentUserModel cum = new CurrentUserModel();
                        cum.Vorname = user.Firstname;
                        cum.Nachname = user.Lastname;
                        cum.EMail = user.EMail;
                        cum.UserId = user.UserId;
                        cum.IsAdmin = false;
                        var filterResult = filterContext.Result as ViewResult;
                        if (filterResult != null)
                        {
                            filterResult.ViewBag.CurrentUser = cum;
                        }
                    }
                }
            }
        }

    }
}