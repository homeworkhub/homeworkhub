function AppModel() {
    var self = this;

    self.AdminAreaModel = new AdminAreaModel();
    self.NewAccountModel = new AccountModel(null);
}

/* Allgemeines Fehlermodell */

function ErrorsModel() {
    var self = this;

    self.Items = ko.observableArray();

    self.Fill = function (items) {
        self.Items.removeAll();

        for (var n = 0; n < items.length; n++) {
            self.Items.push(items[n]);
        }
    }

    self.HasErrors = function () {
        return self.Items().length > 0;
    }

    self.Clear = function () {
        self.Items.removeAll();
    }
}

/* Registrierungsmodel */

function AccountModel(item) {
    var self = this;

    self.Errors = new ErrorsModel();

    if (item != null) {
        self.Vorname = ko.observable(item.Vorname);
        self.Nachname = ko.observable(item.Nachname);
        self.Password = ko.observable(item.Password);
        self.PasswordWH = ko.observable(item.PasswordWH);
        self.EMail = ko.observable(item.EMail);
        self.EMailWh = ko.observable(item.EMailWh);
    } else {
        self.Vorname = ko.observable('');
        self.Nachname = ko.observable('');
        self.Password = ko.observable('');
        self.PasswordWH = ko.observable('');
        self.EMail = ko.observable('');
        self.EMailWh = ko.observable('');
    }

    self.Merge = function (item) {

        if (item.Vorname != null) {
            self.Vorname(item.Vorname);
        }

        if (item.Nachname != null) {
            self.Nachname(item.Nachname);

        } if (item.Password != null) {
            self.Password(item.Password);
        }

        if (item.PasswordWH != null) {
            self.PasswordWH(item.PasswordWH);
        }

        if (item.EMail != null) {
            self.EMail(item.EMail);
        }

        if (item.EMailWh != null) {
            self.EMailWh(item.EMailWh);
        }

        // Fehler beim Merge bef�llen
        self.Errors.Fill(item.Errors);
    };

    self.Clear = function () {
        self.Vorname('');
        self.Nachname('');
        self.Password('');
        self.PasswordWH('');
        self.EMail('');
        self.EMailWh('');

        self.Errors.Clear();
    };

    self.JsonObj = function () {
        return { Vorname: self.Vorname(), Nachname: self.Nachname(), Password: self.Password(), PasswordWH: self.PasswordWH(), EMail: self.EMail(), EMailWh: self.EMailWh() };
    }
}

/* Administration Model */

function AdminAreaModel() {
    var self = this;

    self.TopicOverviewModel = new TopicOverviewModel();
}

/* Themenverwaltungs Model */

function TopicOverviewModel() {
    var self = this;

    self.CurrentEditModel = new TopicModel(null);
    self.NewTopicModel = new TopicModel(null);

    self.TopicList = new TopicListModel();

    self.Select = function (id) {
        var selectedItem = self.TopicList.Get(id);
        if (selectedItem != null) {
            self.CurrentEditModel.Merge(selectedItem);
        } else {
            self.CurrentEditModel.Clear();
        }
    };

    self.New = function (id) {
        self.NewTopicModel = new TopicModel(null);
    };

}

function TopicListModel() {
    var self = this;

    self.Items = ko.observableArray();

    self.Fill = function (items) {

        self.Items.removeAll();
        for (var n = 0; n < items.length; n++) {
            self.Add(items[n]);
        }
    };

    self.Remove = function (id) {
        for (var n = 0; n < self.Items().length; n++) {
            if (self.Items()[n].TopicId() == id) {
                self.Items.remove(self.Items()[n]);
            }
        }
    };

    self.Add = function (item) {
        self.Items.push(new TopicModel(item));
    };

    self.Get = function (id) {
        for (var n = 0; n < self.Items().length; n++) {
            if (self.Items()[n].TopicId() == id) {
                return self.Items()[n];
            }
        }
        return null;
    };
}

function TopicModel(item) {
    var self = this;

    if (item != null) {
        self.TopicId = ko.observable(item.TopicId);
        self.Name = ko.observable(item.Name);
        self.Description = ko.observable(item.Description);
    } else {
        self.TopicId = ko.observable('');
        self.Name = ko.observable('');
        self.Description = ko.observable('');
    }

    self.Merge = function (item) {
        self.TopicId(item.TopicId());
        self.Name(item.Name());
        self.Description(item.Description());
    };

    self.Clear = function () {
        self.TopicId('');
        self.Name('');
        self.Description('');
    };

    self.JsonObj = function () {
        return { TopicId: self.TopicId(), Name: self.Name(), Description: self.Description() };
    }
}



// Instanz des Anwendungsmodells
var model = new AppModel();