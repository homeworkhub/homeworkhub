﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Dbz.HomeworkHub.Frontend.Common;

namespace Dbz.HomeworkHub.Frontend.Controllers
{
    /// <summary>
    /// Controller steuert die Verwaltung der Benutzeraccounts und Logins
    /// </summary>
    public class AccountController : BaseController
    {
        /// <summary>
        /// Registriert einen neuen Benutzer
        /// </summary>
        /// <param name="account">Accountdaten welche für die Registrierung verwenet werden</param>
        /// <returns>JSON Ergebnis mit den Daten der Registrierung und einem Statuscode</returns>
        public JsonResult Register(Models.AccountModel account)
        {
            // Alle aktuellen Fehler löschen
            account.Errors.Clear();

            // Erneut prüfen
            if (String.IsNullOrEmpty(account.Vorname))
                account.Errors.Add("Vorname muss angegeben werden.");
            if (String.IsNullOrEmpty(account.Nachname))
                account.Errors.Add("Nachname muss angegeben werden.");
            if (String.IsNullOrEmpty(account.EMail) || !account.EMail.IsValidEmail() || account.EMail.CompareTo(account.EMailWh) != 0)
                account.Errors.Add("Es wurde keine E-Mailaddresse angegeben oder die E-Mailaddresse hat ein ungültiges Format oder stimmt nicht mit dem Feld E-Mailwiderholung überein.");
            if (!account.Password.IsPasswordFormat() || account.Password.CompareTo(account.PasswordWH) != 0)
                account.Errors.Add("Es muss ein Passwort eingegeben werden. Das Passwort muss mit dem Feld Passwortwiederholung übereinstimmen und mindesens acht Zeichen lang sein.");
            if (!String.IsNullOrEmpty(account.EMail) && BusinessObjects.Users.GetUserByEMail(account.EMail) != null)
                account.Errors.Add("Ein Benutzer mit der E-Mailaddresse existiert bereits.");

            // Wenn kein Fehler dann Benutzer anmelden
            if (!account.ErrorOccured)
            {
                // Passwort salten und hashen
                string saltedPassword = account.Password.SaltAndHash();

                Dbz.HomeworkHub.Core.Entities.User newUser = BusinessObjects.Users.New(account.EMail, account.Vorname, account.Nachname, saltedPassword);
            }

            JsonResult jr = new JsonResult();
            jr.Data = account;
            jr.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return jr;
        }

        /// <summary>
        /// Führt die Anmeldung eines Benutzers anhand seiner E-Mailaddresse und seinem Passwort durch
        /// </summary>
        /// <param name="username">Benutzername welcher für die Anmeldung verwendet wird</param>
        /// <param name="password">Passwort welches für die Anmeldung verwendet wird</param>
        /// <returns>Startview</returns>
        public ActionResult Login(string username, string password)
        {
            Dbz.HomeworkHub.Core.Entities.User user = BusinessObjects.Users.GetUserByEMailAndPassword(username, password.SaltAndHash());
            if (user != null)
            {
                FormsAuthentication.SetAuthCookie(user.EMail, false);
            }

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Meldet den aktuell angemeldete Benutzer ab
        /// </summary>
        /// <returns>Startview</returns>
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}