﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dbz.HomeworkHub.Frontend.Controllers
{
    /// <summary>
    /// Kontroller für die Verwaltung von Hausaufgaben
    /// </summary>
    public class HomeworkController : BaseController
    {
        /// <summary>
        /// Liefert die Übersicht aller offenen Hausaufgaben
        /// </summary>
        /// <returns>Ansicht aller offenen Hausaufgaben</returns>
        public ActionResult Open()
        {
            List<Dbz.HomeworkHub.Core.Entities.Homework> themen = BusinessObjects.Homeworks.GetOpen();
            ViewBag.Aufgaben = themen;
            return View();
        }

        /// <summary>
        /// Liefert die Übersicht aller Hausaufgaben im System
        /// </summary>
        /// <returns>Ansicht aller Hausaufgaben</returns>
        public ActionResult All()
        {
            List<Dbz.HomeworkHub.Core.Entities.Homework> themen = BusinessObjects.Homeworks.GetAll();
            ViewBag.Aufgaben = themen;
            return View();
        }
        public ActionResult My()
        {
            List<Dbz.HomeworkHub.Core.Entities.Homework> themen = BusinessObjects.Homeworks.GetForCreator(CurrentUser.UserId);
            ViewBag.Aufgaben1 = themen;

            List<Dbz.HomeworkHub.Core.Entities.Homework> themen2 = BusinessObjects.Homeworks.GetForSubscriber(CurrentUser.UserId);
            ViewBag.Aufgaben2 = themen2;
            return View();
        }

        /// <summary>
        /// Liefert die Seite zum erstellen neuer Hausaufgaben
        /// </summary>
        /// <returns>Ansicht zum Erstellen einer Hausaufgabe</returns>
        public ActionResult New()
        {
            ViewBag.Topics = BusinessObjects.Topics.GetAll();
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Create(string name, DateTime? lastSubmitDate, int? neededTopicRank, int? credits, string beschreibung, int? topic)
        {
            // Fehler überprüfen
            List<string> errorList = new List<string>();
            if (String.IsNullOrEmpty(name))
                errorList.Add("Der Titel der Hausaufgabe muss angegeben werden.");
            if (String.IsNullOrEmpty(beschreibung))
                errorList.Add("Sie müssen eine Beschreibung für die Hausaufgabe angebenen.");
            if (!lastSubmitDate.HasValue)
                errorList.Add("Sie müssen ein spätestens Abgabedatum angeben");
            if (!neededTopicRank.HasValue)
                errorList.Add("Sie müssen einen Mindestrang für die Bearbeitung angeben.");
            if (!topic.HasValue)
                errorList.Add("Sie müssen ein Thema auswählen.");

            if(errorList.Count > 0)
            {
                ViewBag.Errors = errorList;
                return View("New");
            }
            else
            {
                Dbz.HomeworkHub.Core.Entities.Homework homework = BusinessObjects.Homeworks.New(name, lastSubmitDate.Value, neededTopicRank.Value, beschreibung, CurrentUser.UserId, topic.Value);
    
                if(credits.HasValue)
                    homework.CreditPoints = credits.Value;
                
                return RedirectToAction("Open", "Homework");
            }
        }

        public ActionResult Details(int homeworkId)
        {
            Dbz.HomeworkHub.Core.Entities.Homework homework = BusinessObjects.Homeworks.GetOne(homeworkId);
            return View("Details", homework);
        }

        public ActionResult Subscribe(int homeworkId)
        {
            Dbz.HomeworkHub.Core.Entities.Homework homework = BusinessObjects.Homeworks.GetOne(homeworkId);
            homework.Subscriber = CurrentUser;
            homework.SubscriptionDate = DateTime.Now;
            BusinessObjects.Homeworks.Update(homework);

            return RedirectToAction("Open", "Homework");
        }
    }
}
