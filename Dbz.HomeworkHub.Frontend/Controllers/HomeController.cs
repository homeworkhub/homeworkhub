﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dbz.HomeworkHub.Frontend.Controllers
{
    /// <summary>
    /// Startcontroller für die Startseite der Anwendung
    /// </summary>
    public class HomeController : BaseController
    {
        /// <summary>
        /// Controllermethode für die Startseite
        /// </summary>
        /// <returns>Ansicht der Startseite</returns>
        public ActionResult Index()
        {
            return View();
        }
    }
}
