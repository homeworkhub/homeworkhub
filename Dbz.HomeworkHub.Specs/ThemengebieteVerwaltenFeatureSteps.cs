﻿using System;
using TechTalk.SpecFlow;

namespace Dbz.HomeworkHub.Specs
{
    [Binding]
    public class ThemengebieteVerwaltenFeatureSteps
    {
        [Given(@"I am logged in as administrator")]
        public void GivenIAmLoggedInAsAdministrator()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I have entered all required fields with valid data")]
        public void GivenIHaveEnteredAllRequiredFieldsWithValidData()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I have entered unvalid data")]
        public void GivenIHaveEnteredUnvalidData()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I have changed data with valid data")]
        public void GivenIHaveChangedDataWithValidData()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I have changed data with unvalid data")]
        public void GivenIHaveChangedDataWithUnvalidData()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"a Item exists")]
        public void GivenAItemExists()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I press '(.*)'")]
        public void WhenIPress(string p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I should see it in List")]
        public void ThenIShouldSeeItInList()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I Should see validation errors")]
        public void ThenIShouldSeeValidationErrors()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"it should contain modified data")]
        public void ThenItShouldContainModifiedData()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I shouldn't see it in List")]
        public void ThenIShouldnTSeeItInList()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
