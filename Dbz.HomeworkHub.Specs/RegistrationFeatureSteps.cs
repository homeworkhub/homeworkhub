﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Firefox;
using System;
using TechTalk.SpecFlow;

namespace Dbz.HomeworkHub.Specs
{
    [Binding]
    public class RegistrationFeatureSteps
    {
        FirefoxDriver driver = new FirefoxDriver();

        [Given(@"I am not logged in")]
        public void GivenIAmNotLoggedIn()
        {
            driver.Url = "http://localhost:10251/";
            driver.Navigate();
        }


        [Given(@"I press RegisterNow")]
        public void GivenIPressRegisterNow()
        {
            driver.FindElementById("loginFormRegisterNowButton").Click();
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(7));
        }
        
        [Given(@"filled in my vorname for registration")]
        public void GivenFilledInMyVornameForRegistration()
        {
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(7));
            driver.FindElementById("registerUserDialogVorname").SendKeys("SeleniumRegisterVorname");
        }
        
        [Given(@"filled in my nachname for registration")]
        public void GivenFilledInMyNachnameForRegistration()
        {
            driver.FindElementById("registerUserDialogNachname").SendKeys("SeleniumRegisterNachname");
        }
        
        [Given(@"filled in my email for registration")]
        public void GivenFilledInMyEmailForRegistration()
        {
            driver.FindElementById("registerUserDialogEMail").SendKeys("seleniumregisteremail@mail.com");
        }
        
        [Given(@"filled in my emailRepeat for registration")]
        public void GivenFilledInMyEmailRepeatForRegistration()
        {
            driver.FindElementById("registerUserDialogEMailWH").SendKeys("seleniumregisteremail@mail.com");
        }
        
        [Given(@"filled in my password for registration")]
        public void GivenFilledInMyPasswordForRegistration()
        {
            driver.FindElementById("registerUserDialogPasswort").SendKeys("password1");
        }
        
        [Given(@"filled in my passwordRepeat for registration")]
        public void GivenFilledInMyPasswordRepeatForRegistration()
        {
            driver.FindElementById("registerUserDialogPasswortWH").SendKeys("password1");
        }
        
        [Given(@"filled in my email for registration which is already in use")]
        public void GivenFilledInMyEmailForRegistrationWhichIsAlreadyInUse()
        {
            driver.FindElementById("registerUserDialogEMail").SendKeys("seleniumregisteremail@mail.com");
        }
        [Given(@"checked Nutzerbedingungen")]
        public void GivenCheckedNutzerbedingungen()
        {
            driver.FindElementById("registerUserDialogCheckbox").Click();
        }

        
        [When(@"I press Register")]
        public void WhenIPressRegister()
        {
            driver.FindElementById("registerFormNewAccountButton").Click();
        }

        [Then(@"I get a registration affirmation")]
        public void ThenIGetARegistrationAffirmation()
        {
            driver.FindElementById("registerFormNewAccountButton").Click();
        }

        
        [Then(@"there is a new user account")]
        public void ThenThereIsANewUserAccount()
        {
           Assert.IsNull(driver.FindElementById("registerErrors"));
        }
        


        [Then(@"I see a error message")]
        public void ThenISeeAErrorMessage()
        {
            Assert.IsNotNull(driver.FindElementById("registerErrors"));
        }
    }
}
