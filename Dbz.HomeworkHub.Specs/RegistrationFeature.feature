﻿Feature: RegistrationFeature
As a Guest I want to register a new user account.
Scenario: Registration Possible
Given I am not logged in
And I press RegisterNow
And filled in my vorname for registration
And filled in my nachname for registration
And filled in my email for registration
And filled in my emailRepeat for registration
And filled in my password for registration
And filled in my passwordRepeat for registration
And checked Nutzerbedingungen
When I press Register
Then I get a registration affirmation
And there is a new user account
Scenario: Registration not possible
Given I am not logged in
And I press RegisterNow
And filled in my vorname for registration
And filled in my nachname for registration
And filled in my email for registration which is already in use
And filled in my emailRepeat for registration
And filled in my password for registration
And filled in my passwordRepeat for registration
And checked Nutzerbedingungen
When I press Register
Then I see a error message