﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using TechTalk.SpecFlow;

namespace Dbz.HomeworkHub.Specs
{
    [Binding]
    public class LoginFeatureSteps
    {
        FirefoxDriver driver = new FirefoxDriver();

        [Given(@"I'm not logged in")]
        public void GivenIMNotLoggedIn()
        {
            driver.Url = "http://localhost:10251/";
            driver.Navigate();
        }
        
        [Given(@"filled in my email")]
        public void GivenFilledInMyEmail()
        {
            driver.FindElementById("username").SendKeys("seleniumtest@donotdelete.com");
        }
        
        [Given(@"filled in my password")]
        public void GivenFilledInMyPassword()
        {
            driver.FindElementById("password").SendKeys("password1");
        }

        [Given(@"filled an invalid password")]
        public void GivenFilledAnInvalidPassword()
        {
            driver.FindElementById("password").SendKeys("passwordInvalid");
        }

        
        [When(@"I press Login")]
        public void WhenIPressLogin()
        {
            driver.FindElementById("smallLoginFormSignIn").Click();
        }
        
        [Then(@"I see a user menu")]
        public void ThenISeeAUserMenu()
        {
            Assert.IsNotNull(driver.FindElementByClassName("dropdown-toggle"));
        }
         
        [Then(@"there is a new User Session")]
        public void ThenThereIsANewUserSession()
        {
            Assert.IsNotNull(driver.Manage().Cookies.GetCookieNamed(".ASPXAUTH"));
            driver.Quit();
        }

        [Then(@"I'm not logged In")]
        public void ThenIMNotLoggedIn()
        {
            Assert.IsNull(driver.Manage().Cookies.GetCookieNamed(".ASPXAUTH"));
        }

        [Then(@"email field is empty")]
        public void ThenEmailFieldIsEmpty()
        {
            Assert.AreEqual(driver.FindElementById("username").Text, "");
        }

        [Then(@"password field is empty")]
        public void ThenPasswordFieldIsEmpty()
        {
            Assert.AreEqual(driver.FindElementById("password").Text, "");
            driver.Quit();
        }

    }
}
