﻿Feature: LoginFeature
As a User I want to start a new user session
so I have to log in
Scenario: Valid Login
Given I'm not logged in
And filled in my email
And filled in my password
When I press Login
Then I see a user menu
And there is a new User Session
Scenario: Invalid Login
Given I'm not logged in
And filled in my email
And filled an invalid password
When I press Login
Then I'm not logged In
And email field is empty
And password field is empty