﻿Feature: ThemengebieteVerwaltenFeature
As a administrator I want to administrate the topics,
so I can create, read, update and delete it.
Scenario: Sucessfull Create
Given I am logged in as administrator
And I have entered all required fields with valid data
When I press 'Create'
Then I should see it in List
Scenario: Unvalid Data Create
Given I am logged in as administrator
And I have entered unvalid data
When I press 'Create'
Then I Should see validation errors
Scenario: Sucessfull Update
Given I am logged in as administrator
And I have changed data with valid data
When I press 'Update'
Then I should see it in List
And it should contain modified data
Scenario: Unvalid Data Update
Given I am logged in as administrator
And I have changed data with unvalid data
When I press 'Update'
Then I Should see validation errors
Scenario: Sucessfull Delete
Given I am logged in as administrator
And a Item exists
When I press 'Delete Item'
Then I shouldn't see it in List
Scenario: Unsucessfull Delete
Given I am logged in as administrator
And a Item exists
When I press 'Delete Item'
Then I Should see validation errors