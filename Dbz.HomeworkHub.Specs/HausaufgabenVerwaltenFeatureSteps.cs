﻿using System;
using TechTalk.SpecFlow;

namespace Dbz.HomeworkHub.Specs
{
    [Binding]
    public class HausaufgabenVerwaltenFeatureSteps
    {
        [Given(@"I am logged in as user")]
        public void GivenIAmLoggedInAsUser()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
