﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dbz.HomeworkHub.Frontend.Models
{
    public class AccountModel : BaseModel
    {
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public string Password { get; set; }
        public string EMail { get; set; }
        public string EMailWh { get; set; }
        public string PasswordWH { get; set; }
    }
}