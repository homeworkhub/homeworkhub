﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Dbz.HomeworkHub.Frontend.Models;

namespace Dbz.HomeworkHub.Frontend.Controllers
{
    /// <summary>
    /// Basiscontroller für alle anderen Controller, stellt für abgeleitete Controller den Zugriff auf die Datenbank bereit
    /// </summary>
    public class BaseController : Controller
    {
        public static Dictionary<HttpRequest, Dbz.HomeworkHub.Core.BusinessObjects> BusinessObjectCache { get; set; }

        /// <summary>
        /// Liefert die Business-Objects anhand des aktuellen Session-Contextes
        /// </summary>
        protected Dbz.HomeworkHub.Core.BusinessObjects BusinessObjects
        {
            get
            {
                if (BusinessObjectCache[System.Web.HttpContext.Current.Request] == null)
                    throw new ApplicationException("Session in diesem Context nicht verfügbar");

                return BusinessObjectCache[System.Web.HttpContext.Current.Request];
            }
        }

        /// <summary>
        /// Liefert ein Businessobject nach ausen anhand des übergebenen Requests
        /// </summary>
        /// <param name="request">Der Request für welchen die Business-Objects verwendet werden sollen.</param>
        /// <returns>Businessobjects</returns>
        internal static Dbz.HomeworkHub.Core.BusinessObjects GetBusinessObject(HttpRequest request)
        {
            if (BusinessObjectCache[System.Web.HttpContext.Current.Request] == null)
                throw new ApplicationException("Session in diesem Context nicht verfügbar");

            return BusinessObjectCache[System.Web.HttpContext.Current.Request];
        }
    }
}
