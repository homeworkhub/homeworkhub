﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dbz.HomeworkHub.Frontend.Controllers
{
    /// <summary>
    /// Kontroller für die Verwaltung von Lösungen
    /// </summary>
    public class SolutionController : BaseController
    {
        /// <summary>
        /// Liefert die Übersicht aller Lösungen eines Benutzers
        /// </summary>
        /// <returns>Ansicht aller Lösungen eines Benutzers</returns>
        public ActionResult Index()
        {
            return View();
        }

    }
}
