﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dbz.HomeworkHub.Frontend.Controllers
{
    /// <summary>
    /// Kontroller für die Verwaltung von Themengebieten
    /// </summary>
    public class TopicController : BaseController
    {
        /// <summary>
        /// Liefert die Standardansicht der Verwaltung von Themengebiete
        /// </summary>
        /// <returns>Ansicht der Themengebiete</returns>
        public ActionResult Index()
        {
            return View(new Models.TopicModel());
        }

        /// <summary>
        /// Liefert eine Liste aller Themengebiete im JSON Format
        /// </summary>
        /// <returns>JSON Liste aller Themengebiete</returns>
        public JsonResult ListTopics()
        {
            List<Dbz.HomeworkHub.Core.Entities.Topic> themen = BusinessObjects.Topics.GetAll();

            JsonResult jr = new JsonResult();
            jr.Data = themen;
            jr.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return jr;
        }

        /// <summary>
        /// Erstellt ein neues Themengebiet
        /// </summary>
        /// <param name="topic">Das Modell des Themengebiets welches angelegt werden soll</param>
        /// <returns>JSON Liste aller Themengebiete</returns>
        public JsonResult New(Dbz.HomeworkHub.Core.Entities.Topic topic)
        {
            Dbz.HomeworkHub.Core.Entities.Topic newTopic = BusinessObjects.Topics.New(topic.Name);
            newTopic.Description = topic.Description;
            BusinessObjects.Topics.Update(newTopic);

            return ListTopics();
        }

        /// <summary>
        /// Speichert ein bestehendes Themengebit
        /// </summary>
        /// <param name="topic">Das Modell des Themengebiets welches gespeichert / aktualisiert werden soll</param>
        /// <returns>JSON Liste aller Themengebiete</returns>
        public JsonResult Update(Dbz.HomeworkHub.Core.Entities.Topic topic)
        {
            if (topic.TopicId > 0)
                BusinessObjects.Topics.Update(topic);

            return ListTopics();
        }

        /// <summary>
        /// Löscht ein bestehendes Themengebiet
        /// </summary>
        /// <param name="topic">Das Modell des Themengebiets welches gelöscht werden soll</param>
        /// <returns>JSON Liste aller Themengebiete</returns>
        public JsonResult Delete(Dbz.HomeworkHub.Core.Entities.Topic topic)
        {
            if (topic.TopicId > 0)
                BusinessObjects.Topics.Delete(topic);

            return ListTopics();
        }
    }
}
