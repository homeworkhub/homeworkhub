﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dbz.HomeworkHub.Frontend.Controllers
{
    /// <summary>
    /// Controller für die Verwaltung der Administration
    /// </summary>
    public class AdminController : BaseController
    {
        /// <summary>
        /// Standardmethode für Startseite der Administration
        /// </summary>
        /// <returns>Startansicht der Administration</returns>
        public ActionResult Index()
        {
            return View();
        }
    }
}
