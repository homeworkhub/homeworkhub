﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dbz.HomeworkHub.Frontend.Controllers
{
    /// <summary>
    /// Kontroller für die Verwaltung von Hausaufgaben
    /// </summary>
    public class HomeworkController : BaseController
    {
        /// <summary>
        /// Liefert die Übersicht aller offenen Hausaufgaben
        /// </summary>
        /// <returns>Ansicht aller offenen Hausaufgaben</returns>
        public ActionResult Open()
        {
            return View();
        }

        /// <summary>
        /// Liefert die Übersicht aller Hausaufgaben im System
        /// </summary>
        /// <returns>Ansicht aller Hausaufgaben</returns>
        public ActionResult All()
        {
            return View();
        }

    }
}
