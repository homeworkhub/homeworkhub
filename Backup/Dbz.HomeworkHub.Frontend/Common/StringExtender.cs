﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Security.Cryptography;


namespace Dbz.HomeworkHub.Frontend.Common
{
    /// <summary>
    /// Klasse erweitert die C#-String Klasse um spezifische Methoden
    /// </summary>
    public static class StringExtender
    {
        /// <summary>
        /// Überprüft ob ein String eine Valide E-Mailaddresse ist
        /// </summary>
        /// <param name="email">Der String der überprüft wird</param>
        /// <returns><c>true</c> wenn es sich um eine gültige Mail-Addresse handelt ansonsten <c>false</c></returns>
        public static bool IsValidEmail(this string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Überprüft ob das 
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool IsPasswordFormat(this string password)
        {
            // Passwort darf nicht leer sein, es muss mindestens 8 Zeichen beinhalten.
            if (!String.IsNullOrEmpty(password) && password.Length > 8)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Hasht einen String mit einem Hardcodierten Salt
        /// </summary>
        /// <param name="str">Der String der gehashed werden soll</param>
        /// <returns>Gehashte Version des Eingabestrings</returns>
        public static string SaltAndHash(this string str)
        {
            string salt = "Do you see any Teletubbies in here?";

            var hmacMD5 = new HMACMD5(salt.StringToByteArray());
            var saltedHash = hmacMD5.ComputeHash(str.StringToByteArray());
            return saltedHash.ByteArrayToString();
        }

        /// <summary>
        /// Wandelt einen String in ein Byte-Array
        /// </summary>
        /// <param name="str">Der umzuwandelnde String</param>
        /// <returns>Das Bytearray der Umwandlung</returns>
        private static byte[] StringToByteArray(this string str)
        {
            System.Text.UnicodeEncoding enc = new System.Text.UnicodeEncoding();
            return enc.GetBytes(str);
        }

        /// <summary>
        /// Wandelt ein Byte-Array in einen String
        /// </summary>
        /// <param name="arr">Das zu wandelten Byte-Array</param>
        /// <returns>Der String aus der Umwandlung</returns>
        private static string ByteArrayToString(this byte[] arr)
        {
            System.Text.UnicodeEncoding enc = new System.Text.UnicodeEncoding();
            return enc.GetString(arr);
        }
    }
}